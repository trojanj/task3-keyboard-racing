import { createElement, addClass, removeClass, removeSpaces } from './helper.mjs';

const username = sessionStorage.getItem('username');
const createRoomButton = document.getElementById('create-room-button');
const roomsContainer = document.getElementById('rooms-container');
const roomsPage = document.getElementById('rooms-page');
const gamePage = document.getElementById('game-page');
const userList = document.getElementById('user-list');
const backToRoomsBtn = document.getElementById('back-to-rooms');
const readyBtn = document.getElementById('ready-btn');
const gameScreen = document.getElementById('game-screen');
let charIndex = 0;
let timerId;
let onKeyDownHandler;

if (!username) {
  window.location.replace('/login');
}

const socket = io('http://localhost:3002', { query: { username } });

const createRoomHandler = () => {
  let roomName = prompt('Input room name:');

  if (!roomName) return;

  if (roomName.length > 10) {
    roomName = roomName.slice(0, 10);
  }
  roomName = roomName.toLowerCase();
  socket.emit('CREATE_ROOM', roomName);
}

const backToRooms = () => {
  socket.emit('DELETE_ROOMUSER');

  removeClass(roomsPage, 'display-none');
  addClass(gamePage, 'display-none');
}

const readyHandler = () => {
  socket.emit('USER_READY');
  readyBtn.textContent === 'Ready'
    ? readyBtn.textContent = 'Not ready'
    : readyBtn.textContent = 'Ready';
}

createRoomButton.addEventListener('click', createRoomHandler);
backToRoomsBtn.addEventListener('click', backToRooms);
readyBtn.addEventListener('click', readyHandler);

const onUsernameError = message => {
  window.alert(message);
  sessionStorage.removeItem('username');
  window.location.replace('/login');
}

const alertMessage = message => {
  window.alert(message);
}

const createRoom = roomName => {
  const roomId = removeSpaces(roomName);
  const roomItem = createElement({
    tagName: 'div',
    className: 'room-item',
    attributes: {
      id: roomId
    }
  });
  const p = createElement({ tagName: 'p', textContent: '1 user connected' });
  const h2 = createElement({ tagName: 'h2', textContent: roomName });
  const button = createElement({ tagName: 'button', className: 'btn no-select', textContent: 'Join' });

  const onJoinRoom = () => {
    socket.emit('JOIN_ROOM', roomName);
  };
  button.addEventListener('click', onJoinRoom);

  roomItem.append(p, h2, button);
  return roomItem;
}

const updateRooms = rooms => {
  const allRooms = rooms.map(createRoom);
  roomsContainer.append(...allRooms);
}

const addRoom = ({roomName, socketId}) => {
  const room = createRoom(roomName);
  roomsContainer.append(room);
  if (socketId) {
    socket.emit('JOIN_ROOM', roomName);
  }
}

const updateUsersNumber = ({ roomName, length }) => {
    const currentRoom = document.getElementById(removeSpaces(roomName));
    if (currentRoom) {
      const p = currentRoom.querySelector('p');
      p.textContent = length > 1 ? `${length} users connected` : `1 user connected`;
    }
}

const createUserBlock = (username, readyUsers, user) => {
  const userBlock = createElement({
    tagName: 'div',
    className: 'user-block',
    attributes: {
      id: user
    }
  })
  const usernameBlock = createElement({
    tagName: 'div',
    className: 'user-name',
    attributes: {
      id: user + '-name'
    },
    textContent: user === username ? user + ' (you)' : user
  })
  const progressBar = createElement({
    tagName: 'div',
    className: 'progress-bar'
  })
  const indicator = createElement({
    tagName: 'div',
    className: 'indicator'
  })

  if (readyUsers.find(readyUser => readyUser === user)) {
    addClass(usernameBlock, 'ready');
  }

  progressBar.append(indicator);
  userBlock.append(usernameBlock, progressBar);
  return userBlock;
}

const joinRoomDone = ({ roomName, roomUsers, username, readyUsers }) => {
  addClass(roomsPage, 'display-none');
  removeClass(gamePage, 'display-none');

  const h1 = gamePage.querySelector('h1');
  h1.textContent = roomName;

  const allUserBlocks = roomUsers.map(createUserBlock.bind(null, username, readyUsers));

  readyBtn.textContent = 'Ready';
  userList.innerHTML = '';
  userList.append(...allUserBlocks);
}

const deleteRoomFromList = roomName => {
  const roomItem = document.getElementById(removeSpaces(roomName));
  if (roomItem) {
    roomItem.remove();
  }
}

const updateRoom = ({ deletedUser, newUser, readyUser, username, percentageWidth }) => {
  if (deletedUser) {
    document.getElementById(deletedUser).remove();
  }
  if (newUser) {
    const newUserBlock = createUserBlock(null, [], newUser);
    userList.append(newUserBlock);
  }
  if (readyUser) {
    const usernameBlock = document.getElementById(username + '-name');
    usernameBlock.classList.contains('ready')
      ? removeClass(usernameBlock, 'ready')
      : addClass(usernameBlock, 'ready')
  }
  if (percentageWidth) {
    const indicator = document.getElementById(username).querySelector('.indicator');
    indicator.style.width = percentageWidth + '%';
    if (percentageWidth === 100) {
      addClass(indicator, 'lime');
    }
  }
}

const showResults = results => {
  clearTimeout(timerId);

  const winner = results[0];
  const modal = document.getElementById("modal");
  const span = document.getElementsByClassName("close")[0];
  const h2 = modal.querySelector('h2');
  const ol = modal.querySelector('ol');

  const listItems = results.map(user => {
    const li = createElement({tagName: 'li', textContent: user});
    return li;
  })

  h2.textContent = username === winner ? 'Congratulations! You won!' : `${winner} won!`
  ol.append(...listItems);
  modal.style.display = 'block';

  const closeModal = () => {
    socket.emit('FINISH_GAME');
    modal.style.display = "none";
    ol.innerHTML = '';
    document.getElementById('time-left').remove();
    document.getElementById('game-text').remove();
    backToRoomsBtn.style.visibility = 'visible';
    readyBtn.textContent = 'Ready';
    readyBtn.style.display = 'block';
    charIndex = 0;
    const indicators = document.querySelectorAll('.indicator');
    indicators.forEach(indicator => {
      if (indicator.style.width === '100%') {
        removeClass(indicator, 'lime');
      }
      indicator.style.width = '0';
    })
  }

  const onSpanClick = () => {
    closeModal();
    span.removeEventListener('click', onSpanClick);
  }

  const onWindowClick = event => {
    if (event.target == modal) {
      closeModal();
      window.removeEventListener('click', onWindowClick);
    }
  }

  span.addEventListener('click', onSpanClick)

  window.addEventListener('click', onWindowClick)
}

const keyDownHandler = (text, gameText, indicator, event) => {
  if (event.key === text[charIndex]) {
    charIndex = charIndex + 1;
    const textDone = `<span class="text-done">${text.slice(0, charIndex)}</span>`;
    const nextChar = `<span class="next-char">${text[charIndex] || ''}</span>`
    const textUndone = `<span>${text.slice(charIndex + 1)}</span>`;
    const percentageWidth = charIndex / text.length * 100;

    socket.emit('UPDATE_USER_PROGRESS', charIndex);

    gameText.innerHTML = textDone + nextChar + textUndone;
    indicator.style.width = percentageWidth + '%';

    if (percentageWidth === 100) {
      addClass(indicator, 'lime');
    }

    if (charIndex >= text.length) {
      document.removeEventListener('keydown', onKeyDownHandler);
      socket.emit('USER_FINISHED', username);
    }
  }
}

const timeLeftCallback = (i, SECONDS_FOR_GAME, timeLeft) => {
  timeLeft.textContent = `${SECONDS_FOR_GAME - i} seconds left`;
  if (SECONDS_FOR_GAME - i > 0) {
    i++;
    timerId = setTimeout(timeLeftCallback.bind(null, i, SECONDS_FOR_GAME, timeLeft), 1000)
  } else {
    document.removeEventListener('keydown', onKeyDownHandler);
    socket.emit('TIME_OUT');
  }
}

const startGame = (text, SECONDS_FOR_GAME, timer) => {
  let i = 1;
  const gameText = createElement({
    tagName: 'div',
    className: 'game-text',
    attributes: {id: 'game-text'},
    textContent: text
  });
  const timeLeft = createElement({
    tagName: 'span',
    className: 'time-left',
    attributes: {
      id: 'time-left'
    },
    textContent: `${SECONDS_FOR_GAME} seconds left`
  })
  const indicator = document.getElementById(username).querySelector('.indicator');

  timer.remove();

  onKeyDownHandler =  keyDownHandler.bind(null, text, gameText, indicator);
  document.addEventListener('keydown', onKeyDownHandler);

  gameScreen.append(timeLeft, gameText);

  timerId = setTimeout(timeLeftCallback.bind(null, i, SECONDS_FOR_GAME, timeLeft), 1000);
}

const startTimer = ({ SECONDS_TIMER_BEFORE_START_GAME, randomId, SECONDS_FOR_GAME }) => {
  let i = 1;
  backToRoomsBtn.style.visibility = 'hidden';
  readyBtn.style.display = 'none';

  const timer = createElement({
    tagName: 'span',
    className: 'game-timer',
    attributes: {
      id: 'game-timer'
    },
    textContent: String(SECONDS_TIMER_BEFORE_START_GAME)
  })
  gameScreen.append(timer);


  const timeoutCallback = () =>{
    timer.textContent = String(SECONDS_TIMER_BEFORE_START_GAME - i);
    if (SECONDS_TIMER_BEFORE_START_GAME - i > 0) {
      setTimeout(timeoutCallback, 1000)
    } else {
      fetch(`/game/texts/${randomId}`)
        .then(response => response.json())
        .then(data => startGame(data.text, SECONDS_FOR_GAME, timer))
        .catch(e => console.log(e))
    }
    i++;
  }

  setTimeout(timeoutCallback, 1000)
}

socket.on('USERNAME_ERROR', onUsernameError);
socket.on('UPDATE_ROOMS', updateRooms);
socket.on('ROOMNAME_ERROR', alertMessage);
socket.on('ADD_ROOM', addRoom);
socket.on('JOIN_ROOM_DONE', joinRoomDone);
socket.on('MAX_USERS_ERROR', alertMessage);
socket.on('UPDATE_USERS_NUMBER', updateUsersNumber);
socket.on('DELETE_ROOM_FROM_LIST', deleteRoomFromList);
socket.on('UPDATE_ROOM', updateRoom);
socket.on('START_TIMER', startTimer);
socket.on('SHOW_RESULTS', showResults)

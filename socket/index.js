import { MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME } from './config';
import { texts } from '../data';
import { getRandomInt } from './helper';
const activeUsersMap = new Map(); // [user, room]
const roomsMap = new Map(); // [room, [users...]]
const readyUsersMap = new Map(); // [room, [readyUsers...]]
const activeRooms = [];
const finishedUsersMap = new Map(); // [room, [finishedUsers...]]
const usersProgressMap = new Map(); // [room, {username: progressLength, fullLength: number}]

export default io => {
  io.on("connect", socket => {
    const username = socket.handshake.query.username;

    if (activeUsersMap.has(username)) {
      const message = 'Such username already exists. Please choose another one.';
      socket.emit('USERNAME_ERROR', message);
    } else if (username !== 'null') {
      activeUsersMap.set(username, null);
    }

    if (roomsMap.size) {
      const rooms = [ ...roomsMap.keys() ];

      if (activeRooms.length) {
        activeRooms.forEach(activeRoom => {
          const ind = rooms.indexOf(activeRoom);
          rooms.splice(ind, 1);
        })
      }

      socket.emit('UPDATE_ROOMS', rooms);
    }

    socket.on('CREATE_ROOM', roomName => {
      if (roomsMap.has(roomName)) {
        const message = 'Room with such name already exists.';
        socket.emit('ROOMNAME_ERROR', message);
        return;
      }
      roomsMap.set(roomName, []);
      readyUsersMap.set(roomName, []);

      socket.broadcast.emit('ADD_ROOM', {roomName});
      socket.emit('ADD_ROOM', {roomName, socketId: socket.id})
    })

    socket.on('JOIN_ROOM', roomName => {
      const roomUsers = roomsMap.get(roomName);

      if (roomUsers.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
        const message = 'Maximum users in the room. Please choose another one.'
        socket.emit('MAX_USERS_ERROR', message);
        return;
      }

      const readyUsers = readyUsersMap.get(roomName);
      roomUsers.push(username);
      roomsMap.set(roomName, roomUsers);
      activeUsersMap.set(username, roomName);

      if (roomUsers.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
        activeRooms.push(roomName);
        io.emit('DELETE_ROOM_FROM_LIST', roomName);
      }

      socket.join(roomName, () => {
        socket.emit('JOIN_ROOM_DONE', {roomName, roomUsers, username, readyUsers});
        socket.to(roomName).emit('UPDATE_ROOM', {newUser: username});
        if (roomUsers.length > 1) {
          io.emit('UPDATE_USERS_NUMBER', {roomName, length: roomUsers.length})
        }
      });
    })

    socket.on('DELETE_ROOMUSER', () => {
      const roomName = activeUsersMap.get(username);
      let roomUsers = roomsMap.get(roomName).filter(user => user !== username);

      activeUsersMap.set(username, null);
      socket.leave(roomName);

      // No users in room - delete room, else update users in room
      if (!roomUsers.length) {
        roomsMap.delete(roomName);
        readyUsersMap.delete(roomName);
        io.emit('DELETE_ROOM_FROM_LIST', roomName);
      } else {
        const readyUsers = readyUsersMap.get(roomName);

        roomsMap.set(roomName, roomUsers);
        readyUsersMap.set(roomName, readyUsers.filter(user => user !== username));

        io.to(roomName).emit('UPDATE_ROOM', {deletedUser: username});
        io.emit('UPDATE_USERS_NUMBER', {roomName, length: roomUsers.length});
      }
    })

    socket.on('USER_READY', () => {
      const roomName = activeUsersMap.get(username);
      let readyUsers = readyUsersMap.get(roomName);

      if (readyUsers.find(user => user === username)) {
        readyUsersMap.set(roomName, readyUsers.filter(user => user !== username))
      } else {
        readyUsersMap.set(roomName, [...readyUsers, username]);
        readyUsers = [...readyUsers, username];
      }

      io.to(roomName).emit('UPDATE_ROOM', {readyUser: true, username});

      if (readyUsers.length === roomsMap.get(roomName).length && activeRooms.includes(roomName)) {
        const randomId = getRandomInt(0, texts.length - 1);

        const progressData = { fullLength: texts[randomId].length };
        readyUsers.forEach(readyUser => {
          progressData[readyUser] = 0;
        })
        usersProgressMap.set(roomName, progressData);

        io.in(roomName).emit('START_TIMER', {SECONDS_TIMER_BEFORE_START_GAME, randomId, SECONDS_FOR_GAME});
      }
    })

    socket.on('UPDATE_USER_PROGRESS', progressLength => {
      const roomName = activeUsersMap.get(username);
      const progressData = usersProgressMap.get(roomName);
      progressData[username] = progressLength;
      usersProgressMap.set(roomName, progressData);

      const percentageWidth = progressLength / progressData.fullLength * 100;
      socket.to(roomName).emit('UPDATE_ROOM', ({ username, percentageWidth }))
    })

    socket.on('USER_FINISHED', username => {
      const roomName = activeUsersMap.get(username);
      let finishedUsers = [];

      if (finishedUsersMap.has(roomName)) {
        finishedUsers = finishedUsersMap.get(roomName);
        finishedUsers.push(username);
        finishedUsersMap.set(roomName, finishedUsers);
      } else {
        finishedUsersMap.set(roomName, [username]);
        finishedUsers.push(username)
      }

      if (finishedUsers.length === roomsMap.get(roomName).length) {
        io.in(roomName).emit('SHOW_RESULTS', finishedUsers);
      }
    })

    socket.on('TIME_OUT', () => {
      const roomName = activeUsersMap.get(username);
      const usersProgress = usersProgressMap.get(roomName);
      const finishedUsers = finishedUsersMap.get(roomName) || [];
      const activeUsers = roomsMap.get(roomName);
      const unfinishedUsers = activeUsers
        .filter(activeUser => !finishedUsers.includes(activeUser))
        .sort((a, b) => usersProgress[b] - usersProgress[a]);
      const results = [...finishedUsers, ...unfinishedUsers];

      socket.emit('SHOW_RESULTS', results);
    })

    socket.on('FINISH_GAME', () => {
      const roomName = activeUsersMap.get(username);
      if (finishedUsersMap.has(roomName)) {
        finishedUsersMap.delete(roomName);
      }
      if (usersProgressMap.has(roomName)) {
        usersProgressMap.delete(roomName);
      }
      readyUsersMap.set(roomName, []);

      io.to(roomName).emit('UPDATE_ROOM', { username, readyUser: true });
    })

    socket.on('disconnect', () => {
      const roomName = activeUsersMap.get(username);

      // User is in room - leave room and delete user from roomsMap
      if (roomName) {
        socket.leave(roomName);
        const roomUsers = roomsMap.get(roomName).filter(user => user !== username);

        // No users in room - delete room, else update users in room
        if (!roomUsers.length) {
          roomsMap.delete(roomName);
          readyUsersMap.delete(roomName);
          io.emit('DELETE_ROOM_FROM_LIST', roomName);
        } else {
          const readyUsers = readyUsersMap.get(roomName);

          roomsMap.set(roomName, roomUsers);
          readyUsersMap.set(roomName, readyUsers.filter(user => user !== username));

          io.to(roomName).emit('UPDATE_ROOM', {deletedUser: username});
          io.emit('UPDATE_USERS_NUMBER', {roomName, length: roomUsers.length});
        }
      }

      activeUsersMap.delete(username);
    })
  });
};
